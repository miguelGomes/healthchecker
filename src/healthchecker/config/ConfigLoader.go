package config

import (
	"healthchecker/model"
	"io/ioutil"
	"gopkg.in/yaml.v2"
)

var filePath = "src/healthchecker/config/config.yaml"

func FilePath(newFilePath string) {
	filePath = newFilePath
}

func LoadConfig() *model.HealthCheckerConfig {

	data, error := ioutil.ReadFile(filePath)
	if error != nil {
		//TODO: Tread this error
		panic(error)
	}

	appConfig := model.HealthCheckerConfig{}

	yaml.Unmarshal(data, &appConfig)

	return &appConfig
}
