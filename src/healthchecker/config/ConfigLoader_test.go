package config_test

import (
	"healthchecker/config"
	"path/filepath"
	"testing"
	"healthchecker/model"
	"time"
	"reflect"
	"flag"
	"io/ioutil"
	"gopkg.in/yaml.v2"
)

var update = flag.Bool("update", false, "update .golden files")

var goldenFilePath = filepath.Join("testdata", "configFile"+".golden")

func init() {

	config.FilePath(goldenFilePath)
}

func TestLoadConfig(t *testing.T) {

	appConfig := &model.HealthCheckerConfig{
		ApplicationInfo: []model.ApplicationInfo{
			{
				Name:           "Test",
				Description:    "",
				HealthCheckUrl: "https://www.google.pt",
				Online:         false,
				LastCheck:      time.Date(1990, time.May, 31, 0,0 ,0, 0, time.UTC),
				NotificationInfo: model.NotificationInfo{
					UpdateFreq: 10,
				},
			},
		},
	}

	if *update {
		data, _ := yaml.Marshal(appConfig)

		ioutil.WriteFile(goldenFilePath, data, 0644)
	}

	loadedAppConfig := config.LoadConfig()

	if !reflect.DeepEqual(appConfig, loadedAppConfig) {
		t.Error("Configurations must be correctly loaded")
	}
}
