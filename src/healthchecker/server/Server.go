package server

import (
	"net/http"
	"healthchecker/model"
	"encoding/json"
	"log"
)

func StartServer(appConfig *model.HealthCheckerConfig) {
	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		writer.Write([] byte("Call \"/config to have information about services"))
	})

	http.HandleFunc("/config", handleName(appConfig))

	log.Fatal(http.ListenAndServe(":7272", nil))
}

func handleName(appConfig *model.HealthCheckerConfig) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {

		json.NewEncoder(writer).Encode(appConfig.ExternalAppsInfo())
	}
}