package health_test

import (
	"testing"
	"gopkg.in/jarcoal/httpmock.v1"
	"healthchecker/model"
	"time"
	"errors"
	"healthchecker/health"
)

func TestHealthCheck(t *testing.T) {

	tt := []struct {
		Description string
		ErrorCode   int
		Result      bool
	}{
		{"Sucess Response", 200, true},
		{"Error Response", 500, false},
	}

	for _, tc := range tt {

		t.Run(tc.Description, func(t *testing.T) {

			const url = "http://test.com"

			time := time.Now()

			appInfo := &model.ApplicationInfo{
				HealthCheckUrl: url,
				Name:           "Test",
				Description:    "This is a test",
				LastCheck:      time,
			}

			httpmock.Activate()
			defer httpmock.DeactivateAndReset()

			httpmock.RegisterResponder("GET", url,
				httpmock.NewStringResponder(tc.ErrorCode, ""))

			result := health.HealthCheck(appInfo)

			if result != tc.Result {
				t.Errorf("Result mult be %t, got %t", tc.Result, result)
			}

			if appInfo.Online != tc.Result {
				t.Errorf("Online bool must be %t, got %t", tc.Result, appInfo.Online)
			}

			if !appInfo.LastCheck.After(time) {
				t.Error("Update time must be updated")
			}

		})
	}
}

func TestHealthCheckWithException(t *testing.T) {

	const url = "http://test.com"

	time := time.Now()

	appInfo := &model.ApplicationInfo{
		HealthCheckUrl: url,
		Name:           "Test",
		Description:    "This is a test",
		LastCheck:      time,
	}

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("GET", url,
		httpmock.NewErrorResponder(errors.New("error")))

	result := health.HealthCheck(appInfo)

	if result {
		t.Errorf("Result mult be false, got %t", result)
	}

	if appInfo.Online {
		t.Errorf("Online bool must be false, got %t", appInfo.Online)
	}

	if !appInfo.LastCheck.After(time) {
		t.Error("Update time must be updated")
	}

}
