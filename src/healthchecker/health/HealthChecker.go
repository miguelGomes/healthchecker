package health

import (
	"healthchecker/model"
	"net/http"
	"time"
)

func HealthCheck(info *model.ApplicationInfo) bool {

	info.LastCheck = time.Now()

	resp, error := http.Get(info.HealthCheckUrl)
	if error != nil {
		info.Online = false
		return false
	}

	info.Online = resp.StatusCode == 200
	return info.Online
}
