package notifier

import (
	"healthchecker/model"
	"github.com/demisto/slack"
	"fmt"
	"github.com/go-errors/errors"
)

const slackToken = ""

func NotifySlack(appInfo *model.ApplicationInfo)  {

	if appInfo.ShouldNotify() {

		message := slack.PostMessageRequest{
			Channel:  "test",
			Username: "test",
			AsUser:   false,
		}

		s, err := slack.New(slack.SetToken(slackToken))
		text := fmt.Sprintf("@channel Service \"%s\" is down!!!", appInfo.Name)
		message.Text = text
		if err != nil {
			panic(errors.New("pois"))
		}
		s.PostMessage(&message, false)

		appInfo.NotificationSent()
	}
}