package main

import (
	"healthchecker/config"
	"healthchecker/health"
	"healthchecker/model"
	"time"
	"healthchecker/notifier"
	"healthchecker/server"
)

func main() {
	appConfig := config.LoadConfig()

	go func() {
		for {
			checkServices(appConfig)
			time.Sleep(time.Second * 10)
		}
	}()

	server.StartServer(appConfig)
}


func checkServices(appConfig *model.HealthCheckerConfig) {

	for idx := range appConfig.ApplicationInfo {
		health.HealthCheck(&appConfig.ApplicationInfo[idx])
		notifier.NotifySlack(&appConfig.ApplicationInfo[idx])
	}
}
