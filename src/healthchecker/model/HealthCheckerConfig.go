package model

type HealthCheckerConfig struct {
	ApplicationInfo []ApplicationInfo `yaml:"application_info"`
}

func (hcc *HealthCheckerConfig) ExternalAppsInfo() []*AppInfo {

	appInfoResult := []*AppInfo{}

	for idx := range hcc.ApplicationInfo {
		appInfoResult = append(appInfoResult, hcc.ApplicationInfo[idx].ExternalAppInfo())
	}

	return appInfoResult
}