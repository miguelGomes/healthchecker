package model_test

import (
	"testing"
	"healthchecker/model"
	"time"
)

func TestNotificationInfo_ShouldUpdate(t *testing.T) {

	tt := []struct {
		TestName        string
		TimeToSubstract int
		UpdateFreq      int64
		Result          bool
	}{
		{"Updated too long ago", -11, 10, true},
		{"Updated in a very short time", -1, 10, false},
	}

	for _, tc := range tt {

		t.Run(tc.TestName, func(t *testing.T) {

			pastTime := time.Now().Add(time.Duration(tc.TimeToSubstract) * time.Minute)

			notificationInfo := model.NotificationInfo{
				UpdateFreq: tc.UpdateFreq,
				LastUpdate: pastTime,
			}

			result := notificationInfo.ShouldUpdate()

			if result != tc.Result {
				t.Errorf("Last update was %d ago and frequency is %d, so the result should be %t ang got %t", tc.TimeToSubstract, tc.UpdateFreq, tc.Result, result)
			}

		})
	}
}

func TestNotificationInfo_Updated(t *testing.T) {

	now := time.Now()

	notificationInfo := model.NotificationInfo{
		LastUpdate: now,
	}

	notificationInfo.Updated()

	if !notificationInfo.LastUpdate.After(now) {
		t.Error("Notification info should has its value update")
	}
}