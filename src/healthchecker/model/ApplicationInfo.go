package model

import "time"

type ApplicationInfo struct {
	Name             string              `yaml:"name"`
	Description      string              `yaml:"description"`
	HealthCheckUrl   string              `yaml:"health_check_url"`
	Online           bool                `yaml:"online"`
	StatusChanged    bool                `yaml:"status_changed"`
	LastCheck        time.Time           `yaml:"last_check"`
	NotificationInfo NotificationInfo    `yaml:"notification_info"`
}

func (ai *ApplicationInfo) ShouldNotify() bool {
	return ai.StatusChanged || (!ai.Online && ai.NotificationInfo.ShouldUpdate())
}

func (ai *ApplicationInfo) NotificationSent() {
	ai.StatusChanged = false
	ai.NotificationInfo.Updated()
}

func (ai *ApplicationInfo) ExternalAppInfo() *AppInfo {

	return &AppInfo{
		Name:           ai.Name,
		Description:    ai.Description,
		HealthCheckUrl: ai.HealthCheckUrl,
		Online:         ai.Online,
		LastCheck:      ai.LastCheck,
	}
}
