package model

import (
	"time"
)

type NotificationInfo struct {
	UpdateFreq int64     `yaml:"update_freq"` //In minutes
	LastUpdate time.Time `yaml:"last_update"`
}

func (ni *NotificationInfo) ShouldUpdate() bool {
	return ni.LastUpdate.Add(time.Duration(ni.UpdateFreq) * time.Minute).Before(time.Now())
}

func (ni *NotificationInfo) Updated() {
	ni.LastUpdate = time.Now()
}
