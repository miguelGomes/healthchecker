package model

import "time"

type AppInfo struct {
	Name           string              `json:"name"`
	Description    string              `json:"description"`
	HealthCheckUrl string              `json:"health_check_url"`
	Online         bool                `json:"online"`
	LastCheck      time.Time           `json:"last_check"`
}
