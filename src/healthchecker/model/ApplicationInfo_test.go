package model_test

import (
	"testing"
	"healthchecker/model"
	"time"
)

func TestApplicationInfo_NotificationSent(t *testing.T) {

	past := time.Now()

	applicationInfo := model.ApplicationInfo{
		StatusChanged: true,
		NotificationInfo: model.NotificationInfo{
			//TODO: mock this object
			LastUpdate: past,
		},
	}

	applicationInfo.NotificationSent()

	if applicationInfo.StatusChanged {
		t.Error("Application should mark status changed as false as the user was already notified about this status")
	}


}


